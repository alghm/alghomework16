﻿
Graph graph = new Graph(6);
graph.AddEdge(0, 1);
graph.AddEdge(1, 2);
graph.AddEdge(1, 3);
graph.AddEdge(2, 4);
graph.AddEdge(3, 2);
graph.AddEdge(3, 4);
graph.AddEdge(5, 4);

graph.Print();

int[][] levels = graph.Demucron();

for (int i = 0; i < levels.Length; i++)
{
    Console.Write($"Level {i}: ");
    for (int j = 0; j < levels[i].Length; j++)
    {
        Console.Write(levels[i][j] + " ");
    }
    Console.WriteLine();
} 


Console.WriteLine();

public class Graph
{
    private int[,] adjacencyMatrix;
    private int numberOfVertices;

    public Graph(int numberOfVertices)
    {
        this.numberOfVertices = numberOfVertices;
        adjacencyMatrix = new int[numberOfVertices, numberOfVertices];
    }

    public void AddEdge(int from, int to)
    {
        adjacencyMatrix[from, to] = 1;
    }

    public int[][] Demucron()
    {
        int[] verticeNumbers = Enumerable.Range(0, adjacencyMatrix.GetLength(0)).ToArray();
        var bufferMatrix = (int[,])adjacencyMatrix.Clone();

        int[][] result = new int[numberOfVertices][];
        int level = 0;

        while (level <= numberOfVertices)
        {
            int count = 0;
            int[] values = new int[numberOfVertices];
            Array.Fill(values, -1);
            int ind = 0;

            int[] previousLevelVertices = level > 0 ? result[level - 1] : new int[0];

            Console.WriteLine("Level - " + level);
            Print(bufferMatrix);

            foreach (var j in verticeNumbers)
            {
                int sum = 0;
                for (int i = 0; i < numberOfVertices; i++)
                {
                    if (Array.IndexOf(previousLevelVertices, i) != -1)
                    {
                        continue;
                    }
                    sum += bufferMatrix[i, j];
                }

                if (sum == 0)
                {
                    values[ind++] = j;
                    count++;
                }
            }

            if (level < result.Length)
            {
                values = values.Where(x => x > -1).ToArray();
                verticeNumbers = verticeNumbers.Except(values).ToArray();
                result[level] = new int[count];
                Array.Copy(values, 0, result[level], 0, count);

                foreach (var v in values)
                {
                    for (int i = 0; i < numberOfVertices; i++)
                    {
                        bufferMatrix[v, i] = 0;
                    }
                }
            }

            level++;
        }

        return result;
    }

    public void Print()
    {
        for (int i = 0; i < numberOfVertices; i++)
        {
            for (int j = 0; j < numberOfVertices; j++)
            {
                var val = adjacencyMatrix[i, j];
                Console.Write(val + (val > 9 ? " " : " "));
            }
            Console.WriteLine();
        }
    }

    public static void Print(int[,] source)
    {
        for (int i = 0; i < source.GetLength(0); i++)
        {
            for (int j = 0; j < source.GetLength(1); j++)
            {
                var val = source[i, j];
                Console.Write(val + (val > 9 ? " " : " "));
            }
            Console.WriteLine();
        }
    }
}

